//
//  InformationViewController.m
//  Assign3
//
//  Created by Trevor Eckhardt on 5/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//
//  Updated 06/01/15

#import <Foundation/Foundation.h>
#import "InformationViewController.h"
#import "ViewController.h"

@interface InformationViewController : UIViewController
@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat height = self.view.frame.size.height - 65;
    
    NSString* plistRecipe [[[NSString alloc] pathForResource:@"Property List" ofType:@"plist"]];
    NSDictionary* recipeDict [[NSDictionary alloc] initWithContentsOfFile:plistRecipe];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 30)];
    nameLbl.text = recipeDict[plistRecipe.row][@"name"];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    nameLbl.textColor = [UIColor redColor];
    [self.view addSubview:nameLbl];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 65 + (height * 5/48), self.view.frame.size.width, (height * 18/48))];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage:[UIImage imageNamed: recipeDict[plistRecipe.row][@"image"]]];
    [self.view addSubview:iv];
    
    UITextView* labelDesc = [[UITextView alloc]initWithFrame:CGRectMake(0, 65 + (height * 24/48), self.view.frame.size.width, (height * 23/48))];
    labelDesc.text = recipeDict[plistRecipe.row][@"description"];
    labelDesc.textAlignment = NSTextAlignmentLeft;
    labelDesc.textColor = [UIColor blackColor];
    labelDesc.editable = NO;
    [self.view addSubview:labelDesc];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end